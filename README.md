Dubbo分布式项目快速入门案例(需要在Linux配置并开启zookeeper)：
<p>
    dubbo-interface：<br/>
    提取公共interface接口。
</p>
<p>
    dubbo-pojo：<br/>
    提取公共pojo实体类，注意需要序列化的实体类必须实现Serializable接口。
</p>
<p>
    dubbo-service：<br/>
    服务提供者Provider——依赖于dubbo-interface，<br/>
    启动前必须maven install dubbo-interface模块，<br/>
    启动项目使用maven的tomcat插件。
</p>
<p>
    dubbo-web：<br/>
    服务消费者Consumer——依赖于dubbo-interface，<br/>
    启动前必须maven install dubbo-interface模块，<br/>
    启动项目使用maven的tomcat插件。
</p>
<p>
    Linux操作zookeeper( /opt/zookeeper/bin )：<br/>
    启动：./zkServer.sh start <br/>
    停止：./zkServer.sh stop <br/>
    重启：./zkServer.sh restart <br/>
    状态：./zkServer.sh status
</p>