package com.itheima.service.impl;

import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.apache.dubbo.config.annotation.Service;

/**
 * 服务提供者。Provider。<p>
 * apache.dubbo包下。将这个类提供的方法(服务)对外发布。将访问的地址 ip、端口、路径注册到注册中心中<p>
 * 建议在此设置超时时间，因为谁定义服务，谁知道具体情况、具体时长正常。<p>
 * 注意：服务消费方controller设置超时时间后会覆盖服务提供方service的超时时间！！<p>
 */
//@Service //将该类的对象创建出来，放到spring的IOC容器中 bean定义
//@Service(timeout = 3000, retries = 2,version = "v1.0")//当前服务3秒超时，重试2次，一共3次，版本为v1.0
@Service(weight = 100) //设置权重
public class UserServiceImpl implements UserService {
    @Override
    public String sayHello() {
        return "3............";
    }

    /**
     * 查询用户
     *
     * @param id
     * @return
     */
    int i=1;
    @Override
    public User findUserById(int id) {
        System.out.println("3...");
//        System.out.println("服务被调用了："+i++);
        //查询user对象
        User user=new User(1,"zhangsan","123");
        //模拟超时
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }
}
