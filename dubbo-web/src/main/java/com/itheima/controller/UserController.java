package com.itheima.controller;

import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务消费者。Consumer。
 */
@RestController
@RequestMapping("/user")
public class UserController {

    //注入service
//    @Autowired //本地注入
    /**
     * .@Reference： <p>
     * 1.从zookeeper注册中心获取userService的访问url<p>
     * 2.进行远程调用RPC<p>
     * 3.将结果封装为一个代理对象。给变量赋值<p><p>
     * 不建议服务消费方设置超时时间，一是不太合理，二是会覆盖服务提供方设置的超时时间，！！<p>
     * 使用version设置/调用同一个接口的不同版本。<p>
     */
//    @Reference(version = "v2.0") //远程注入 多版本
//    @Reference(loadbalance = "random") //远程注入，负载均衡，双shift —— AbstractLoadBalance四个实现类的name属性
//    @Reference(cluster = "failover") //远程注入，集群容错， 双shift —— Cluster实现类的name属性
    @Reference(mock = "fail:return null") //远程注入，服务降级，  force/fail:return null  不再调用UserService的服务/失败后返回null，不抛异常
    private UserService userService;

    @RequestMapping("/sayHello")
    public String sayHello(){
        return userService.sayHello();
    }

    /**
     * 根据id查询用户信息
     * @param id
     * @return
     */
    int i=1;
    @RequestMapping("/find")
    public User find(int id){
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    System.out.println(i++);
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();
        return userService.findUserById(id);
    }
}
